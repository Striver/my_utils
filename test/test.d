module test;

import core.thread, std.parallelism, std.stdio;
import core.time : seconds;
import std.conv : to;
import std.string : strip;


import my_utils.journal; //, nastroyki;
//import nastroyki_init;

//enum модуль = "test_journal";
enum модуль = "тест_журнала";

enum ФАЙЛ_ЖУРНАЛА = "журнал1.txt";
enum МИЛЛИ = 0.001;

// Определения функций и массива структур параметров для многопоточного тестирования
// журналирования. Если завернуть эти функции и структуры внутрь unittest - 
// это приведёт к ошибке многопоточного запуска шаблона taskPool.amap, поэтому 
// они здесь отдельно.
string тест_функция(string название_потока, float время1, float время2) {
    в_журнал(модуль, УровниЖурнала.Информация, название_потока ~ " начало");
    Thread.sleep(msecs(to!long(время1/МИЛЛИ)));
    в_журнал(модуль, УровниЖурнала.Информация, 
        журнал_многострочное_форматирование(название_потока ~ "\nпосле 1 сна"));
    Thread.sleep(msecs(to!long(время2/МИЛЛИ)));
    в_журнал(модуль, УровниЖурнала.Информация, название_потока ~ " после 2 сна");
    return название_потока;
}

struct Параметры {
    string название_потока;
    float время1;
    float время2;
}

Параметры[] параметры = [Параметры("поток 1", 0.01, 0.13), 
                         Параметры("поток 2", 0.02, 0.04), 
                         Параметры("поток 3", 0.04, 0.05)];

string раскрытие_параметров(Параметры параметры) {
    return тест_функция(параметры.название_потока, параметры.время1, параметры.время2);
}

unittest {

void проверка_строки(File файл_журнала, string проверяемое_значение) {
    float время;
    string строка = файл_журнала.readln();
    время = to!float(строка[0 .. 10].strip);
    assert(время < 1.0);
    assert(время > 0.0);
    assert(строка[10 .. $] == проверяемое_значение);
}    

    import std.file: getSize, exists;
    УровниЖурнала[string] уровни_модулей = [модуль: УровниЖурнала.Отладка];
    инициализация_журнала(ФАЙЛ_ЖУРНАЛА, УровниЖурнала.Отладка, уровни_модулей);
    auto rez = taskPool.amap!раскрытие_параметров(параметры);
    Thread.sleep(msecs(to!long(0.2/МИЛЛИ)));
    остановить_журнал();
    while (true) {
        Thread.sleep(msecs(to!long(0.2/МИЛЛИ)));
        if (!exists(ФАЙЛ_ЖУРНАЛА)) 
            continue;
        if (getSize(ФАЙЛ_ЖУРНАЛА) > 1000)
            break;
    }
    File файл_журнала = File(ФАЙЛ_ЖУРНАЛА, "r");
    проверка_строки(файл_журнала, "  Начало работы журнала\n");
    assert(файл_журнала.readln() == "---------------------------------------------------------------------------\n");
    assert(файл_журнала.readln() == "   Время  |  Модуль       |  Уровень   |           Сообщение \n");
    assert(файл_журнала.readln() == "---------------------------------------------------------------------------\n");
    файл_журнала.readln(); //поток 1-2-3 начало
    файл_журнала.readln(); //поток 1-2-3 начало
    файл_журнала.readln(); //поток 1-2-3 начало
    //проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 1 начало\n");
    //проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 2 начало\n");
    //проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 3 начало\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 1\n");
    assert(файл_журнала.readln() == "                                       | после 1 сна\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 2\n");
    assert(файл_журнала.readln() == "                                       | после 1 сна\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 3\n");
    assert(файл_журнала.readln() == "                                       | после 1 сна\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 2 после 2 сна\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 3 после 2 сна\n");
    проверка_строки(файл_журнала, "|тест_журнала   |Информация  | поток 1 после 2 сна\n");
    assert(файл_журнала.readln() == "");
    assert(файл_журнала.eof());
    файл_журнала.close();
    
}

// Функция тестирующая инициализацию и загрузку настроек, определённых в модуле nastroyki_test
void тестирование_настроек() {
    import nastroyki_test;

	string ТЕСТОВЫЕ_НАСТРОЙКИ = "\n" ~
	"long1 = 4\n" ~
	"херь какая-то = 78\n" ~
	"АМ_string1 = [\"5\":\"+5\", \"что-то\":\"где-то\"]\n" ~
	"# fdgdgdfv\n" ~
	"   string1 = \"все выживут\"\n" ~
	"double1 = a3";		// Свойство должно попасть в ошибочные_переменные
	
	
    assert(настройки.toString == "");
    assert(настройки.загружено == false);
	
    настройки.прочитать_настройки(ТЕСТОВЫЕ_НАСТРОЙКИ);

	assert(настройки.long1 == 4);
	assert(настройки.double1 == 0.3);
	assert(настройки.string1 == "все выживут");
	assert(настройки.М_string1 == ["что-то", "где-то"]);
	assert(настройки.М_long1 == [4, 5]);
	assert(настройки.М_double1 == [0.1, 0.2]);
	assert(настройки.АМ_string1 == ["5":"+5", "что-то":"где-то"]);

    assert(настройки.загружено == true);

    assert(настройки.ошибочные_переменные == ["double1"]);
}

// Функция выводящая на экран результат загрузки настроек, определённых в модуле nastroyki_test
// и сохранённых в файле vars_test.cfg
void вывод_настроек_с_загрузкой() {
    import nastroyki_test;

    writeln("настройки изначально = ");
    writeln(настройки);
    writeln("настройки.загружено = ", настройки.загружено, "\n");

    //настройки.прочитать_настройки_из_файла("vars_test_.cfg", ';');
	//настройки.прочитать_настройки_из_файла("vars_test.cfg");
	//if (настройки.загружено == false)
		настройки.прочитать_настройки_из_файла("../test/vars_test.cfg");

    writeln("настройки после прочтения файла = ");
    writeln(настройки);

    writeln("настройки.загружено = ", настройки.загружено, "\n");

    writeln(настройки.вывод_строк_генерации());	//Что-то выводит только в режиме компиляции debug

    writeln("\nнастройки.ошибочные_переменные = ", настройки.ошибочные_переменные);
}

unittest {
    тестирование_настроек();
}

void main(){
    //вывод_настроек_с_загрузкой();	//Убираем комментарий, чтобы посмотреть, что не так
    };

