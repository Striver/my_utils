module nastroyki_test;

import my_utils.nastroyki_struct;
//import std.meta: AliasSeq;

alias Кортеж_Настроек1 = AliasSeq!( 
  //Тип             наименование    значение по-умолчанию
    long,           "long1",        "2",
    double,         "double1",      "0.3",
    string,         "string1",      "\"все умрут\"",
    string[],       "М_string1",    "[\"что-то\", \"где-то\"]",
    long[],         "М_long1",      "[4, 5]",
    double[],       "М_double1",    "[0.1, 0.2]",
    string[string], "АМ_string1",   "[\"1\":\"-1\", \"2\":\"-2\"]",
    //int,            "int1",         "100"
);

Настройки!Кортеж_Настроек1 настройки;

